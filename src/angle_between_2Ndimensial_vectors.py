""" 
from https://stackoverflow.com/questions/2827393/angles-between-two-n-dimensional-vectors-in-python/13849249#13849249
in order to compute angle differences
date: 27 aout 2018
:note:
"""


import numpy as np

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """
     Returns the angle in radians between vectors 'v1' and 'v2'::
            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def wind_direction_comparison_core(wdir1,wdir2,trigotrick=False):
    """
     :args:
        wdir1 (nd.array): vector 
        wdir2 (nd.array): vector 
    :returns: 
        diff_s1_ecmwf_wind_dir_rad (nd.array): vector 
    """
    u_component_s1 = np.cos(np.radians(wdir1))
    v_component_s1 = np.sin(np.radians(wdir1))
    u_component_ecmwf = np.cos(np.radians(wdir2))
    v_component_ecmwf = np.sin(np.radians(wdir2))
    vec_s1 = [(u,v) for u,v in zip(u_component_s1,v_component_s1) ]
    vec_ecmwf = [(u,v) for u,v in zip(u_component_ecmwf,v_component_ecmwf) ]
    vec_s1 = np.array(vec_s1)
    vec_ecmwf = np.array(vec_ecmwf)
    diff_s1_ecmwf_wind_dir_rad = [angle_between(vec_s1[xx],vec_ecmwf[xx]) for xx in range(len(vec_s1))]
    if trigotrick: #visiblement il ne faut pas le faire...
        diff_s1_ecmwf_wind_dir_rad = 90.0 - np.degrees(diff_s1_ecmwf_wind_dir_rad) #je repasse de trigo -> a geogrpahic
    else:
        diff_s1_ecmwf_wind_dir_rad = np.degrees(diff_s1_ecmwf_wind_dir_rad)
    return diff_s1_ecmwf_wind_dir_rad
