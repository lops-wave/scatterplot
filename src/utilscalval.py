"""

"""
from matplotlib.pyplot import *
from matplotlib import pyplot as plt
import matplotlib.patheffects as path_effects
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pdb
import pandas as pd
import numpy as np
import datetime
import traceback
from scipy.stats import gaussian_kde
from scipy.stats import pearsonr
from scipy.stats import moment
from scipy.ndimage import gaussian_filter1d

from numpy import polyfit
import calendar
import matplotlib.patches as patches
import logging
from angle_between_2Ndimensial_vectors import wind_direction_comparison_core


class VarDisplay(object):
    def __init__(self, varname, displayname=None, unit=None):
        self.varname = varname
        self.displayname = displayname
        self.unit = unit
        

def scatter_df(idf, varname_x, varname_y, varname_z=None, dropna=False, titlestr='',
               alpha=0.5, lw=0, plot_xy=False,tit=None,
               newfig=True, trace_grid=True, savefig_path=None, clearfig=True, vd=None, axes_limit=None, inv_xy=False, **kwargs):
    """

    :param idf:
    :param varname_x:
    :param varname_y:
    :param varname_z:
    :param dropna:
    :param titlestr:
    :param alpha:
    :param lw:
    :param plot_xy:
    :param tit:
    :param newfig:
    :param trace_grid:
    :param savefig_path:
    :param clearfig:
    :param vd:
    :param axes_limit:
    :param inv_xy:
    :param kwargs:
    :return:
    """
    if clearfig: clf()

    if inv_xy:
        varname_x, varname_y = varname_y, varname_x

    if dropna:
        if varname_z:
            tdf = idf[[varname_x, varname_y, varname_z]]
        else:
            tdf = idf[[varname_x, varname_y]]
        df = tdf.dropna()
    else:
        df = idf

    if newfig:
        figure()
    if varname_z:
        scatter(df[varname_x], df[varname_y], c=df[varname_z], lw=lw, alpha=alpha, **kwargs)
        colorbar(label=getlabel(varname_z, vd))
    else:
        scatter(df[varname_x], df[varname_y], lw=lw, alpha=alpha, **kwargs)
    xlabel(getlabel(varname_x, vd)); ylabel(getlabel(varname_y, vd))
    

    if tit is not None:
        title(tit,fontsize= 10)

    if plot_xy:
        maxval = min(df[varname_y].max(), df[varname_x].max())
        minval = max(df[varname_y].min(), df[varname_x].min())
        plot(np.arange(minval, maxval), np.arange(minval, maxval), 'k', lw=1)

    ax_xlim = getVarAxLimit(varname_x, axes_limit)
    if ax_xlim:
        xlim(*ax_xlim)
    ax_ylim = getVarAxLimit(varname_y, axes_limit)
    if ax_ylim:
        ylim(*ax_ylim)
    ax_zlim = getVarAxLimit(varname_z, axes_limit)
    if ax_zlim:
        clim(*ax_zlim)

    if trace_grid:
        grid()
    if savefig_path:
        savefig(savefig_path)

def getVarDisplay(varname, vd=None, create_if_missing=True):
    if not vd:
        global DEFAULT_VARIABLE_DISPLAY
        vd = DEFAULT_VARIABLE_DISPLAY
    if vd and varname in vd:
        return vd[varname]

    if create_if_missing:
        return VarDisplay(varname, varname, None)

    return None


def getlabel(varname, vd=None):
    lbl = varname
    label_vd = getVarDisplay(varname, vd, create_if_missing=False)
    if not label_vd:
        label_vd = VarDisplay(varname, varname, None)
        logging.warning('getlabel : cannot found %s'%(varname))

    lbl = label_vd.displayname
    if label_vd.unit:
        lbl += ' [%s]'%(label_vd.unit)
    return lbl

def getVarAxLimit(varname, al=None):
    if not al:
        global DEFAULT_AXES_LIMITS
        al = DEFAULT_AXES_LIMITS

    if al:
        if varname in al:
            return al[varname]
        else:
            logging.warning('getVarAxLimit : cannot found %s'%(varname))

    return None

def mkdate(datestring):
    return datetime.datetime.strptime(datestring, '%Y%m%d').date()

def get_daily_count(args, df):
    if args.start:
        start = args.start
    else:
        start = df.idxmin()

    if args.stop:
        stop = args.stop
    else:
        (firstday,lastday) = calendar.monthrange(start.year,start.month)
        #stop = df.idxmax()
        stop = datetime.datetime(start.year,start.month,lastday)

    days = pd.date_range(start, stop, closed='left')
    tmpdf = pd.DataFrame(index=days)
    try:
        tmpdf['count'] = df['fdatedt'].resample(rule='1d', how='count')
    except:
        tmpdf['count'] = df['fdatedt'].groupby(pd.Grouper(freq='1D')).count()
    return tmpdf


def hist2d_df(idf, varname_x, varname_y, dropna=False, label='count', res=(100,100), titlestr='',
              cmin=0.1, newfig=True, trace_grid=True, savefig_path=None, clearfig=True, vd=None, axes_limit=None, 
              plot_xy=False, plot_y0=False,
              tit=None, txt=None, pos=None,rango=None,fontsizeannotation=None, **kwargs):


    if clearfig: clf()
    if dropna:
        tdf = idf[[varname_x, varname_y]]
        df = tdf.dropna()
    else:
        df = idf

    if newfig:
        figure()

    tdf = df[[varname_x, varname_y]]
#     print "hist2d_df before dropna",tdf.count()
    tdf = tdf.dropna()
#     print "hist2d_df after dropna",tdf.count()
#     if 'Eff' in varname_x:
#         pdb.set_trace()
#     hist2d(tdf[varname_y], tdf[varname_x], res, range=np.array([[0,10],[0,10]]),cmin=cmin, **kwargs)
    if rango is not None:
        counter,xedges,yedges,img = hist2d(tdf[varname_y], tdf[varname_x], res,cmin=cmin,range=rango, **kwargs)
    else:
        counter,xedges,yedges,img = hist2d(tdf[varname_y], tdf[varname_x], res,cmin=cmin, **kwargs)
    colorbar(label=label)
    xlabel(getlabel(varname_y, vd)); ylabel(getlabel(varname_x, vd))
    if tit is not None:
        title(tit,fontsize=10)
    #annotate('N: %s pts'%(len(tdf.index)), (10,350), xycoords='axes points')

    ax_xlim = getVarAxLimit(varname_y, axes_limit)
    if ax_xlim:
        xlim(*ax_xlim)
        #print ax_xlim
    ax_ylim = getVarAxLimit(varname_x, axes_limit)
    if ax_ylim:
        ylim(*ax_ylim)

    maxval = min(tdf[varname_y].max(), tdf[varname_x].max())
    minval = max(tdf[varname_y].min(), tdf[varname_x].min())
    if plot_xy:
        if ax_xlim:
            minval = np.min(ax_xlim)-5
            maxval = np.max(ax_xlim)+5
        plot(np.arange(minval, maxval), np.arange(minval, maxval), 'k', lw=2)

    if plot_y0:
        if ax_xlim:
            minval = np.min(ax_xlim)
            maxval = np.max(ax_xlim)+5
        plot(np.arange(minval, maxval), np.arange(minval, maxval)*0., 'k', lw=2)
    if fontsizeannotation is not None:
        fontsizeannot = fontsizeannotation
    else:
        fontsizeannot = 10
    if txt:
        if pos:
            annotate(txt,xy=pos,xycoords='axes fraction',fontsize=fontsizeannot)
        else:
            annotate(txt,xy=(0.1,0.8),xycoords='axes fraction',fontsize=fontsizeannot)

    if trace_grid:
        grid(True)
    if savefig_path:
        savefig(savefig_path)
    return xedges,yedges


def map_day_df(idf, varname, day, lon_name='lon', lat_name='lat', regrid_res=0.5):
    ### C'est moche... mais ca fera l'affaire pour le moment.
    ### FIXME : on perd des points s'ils sont superposes au sein d'une meme journee !!!!

    _pts = idf[day]
    _pts = _pts.dropna()

    ### FIXME : on perd des points a -180 ou 180
    #_pts = _pts[(_pts.lon!=180)&(_pts.lon!=-180)]

    vcurvalues = _pts[varname]
    vlon = _pts[lon_name]-(-180)
    vlat = _pts[lat_name]-(-90)

    regrid_res_lon = regrid_res
    regrid_res_lat = regrid_res*2.
    ilat = np.ma.array((np.ma.array(vlat)/regrid_res_lat).round(), dtype=int)
    ilon = np.ma.array((np.ma.array(vlon)/regrid_res_lon).round(), dtype=int)

    latsize = 180
    lonsize = 360

    #print ilon.min(), ilon.max()

    grid = np.ma.masked_all((latsize/regrid_res_lat+1, lonsize/regrid_res_lon+1))
    grid[ilat,ilon] = vcurvalues

    return grid

def map_mean_df(idf, varname, regrid_res=0.5):
    days = np.unique(idf.index.date)[:]
    grid_full = []
    for d in days:
        map_day = map_day_df(idf, varname, d.strftime('%Y%m%d'),  regrid_res=regrid_res)
        map_day = map_day.reshape((1, map_day.shape[0], map_day.shape[1]))
        #print map_day.shape
        grid_full.append(map_day)
    grid_full_stack = np.ma.vstack(list(grid_full))
    gridmean = grid_full_stack.mean(axis=0)
    #print gridmean.shape
    return gridmean

#def basemap_mean_df(idf, varname, regrid_res=0.5, clearfig=True, vd=None, axes_limit=None, fig=None):
#    return basemap_df(idf, varname, regrid_res, clearfig, vd, axes_limit, fig, method='mean')


def basemap_mean_df(idf, varname, regrid_res=0.5, clearfig=True, vd=None, axes_limit=None, fig=None):
    if clearfig: clf()

    if fig is None:
        fig = figure(figsize=(10,6))

    ax = fig.gca()

    map_mean = map_mean_df(idf, varname, regrid_res=regrid_res)
    regrid_res_lon = regrid_res
    regrid_res_lat = regrid_res*2.

    from mpl_toolkits.basemap import Basemap
    m = Basemap(resolution='c',projection='cyl',lat_0=60.)
    lons = np.arange(-180,180+regrid_res_lon,regrid_res_lon)
    lats = np.arange(-90,90+regrid_res_lat,regrid_res_lat)
    #m.imshow(map_mean, origin='lower')
    m.pcolormesh(lons, lats, map_mean)
    #xx, yy = np.meshgrid(lats, lons)
    #yy, xx = m(yy, xx)
    #m.scatter(xx, yy, 1, c=map_mean, lw=0)
    m.drawcoastlines(linewidth=0.2)
    #m.fillcontinents(color='coral', lake_color='aqua', alpha=alpha)
    m.fillcontinents(color='0.1', alpha=0.2)
    #m.drawmapboundary(fill_color='0.1')

    parallels = np.arange(-90, 90, 30)
    meridians = np.arange(-180., 180., 45)
    m.drawparallels(parallels,labels=[1,0,0,0],dashes = [1,4])
    m.drawmeridians(meridians,labels=[0,0,0,1],dashes = [1,4])

    lbl = ''
    lbl_vd = getVarDisplay(varname)
    if lbl_vd.unit:
        lbl = lbl_vd.unit

    divider = make_axes_locatable(ax)
    cax1 = divider.append_axes("bottom", size="5%", pad=.5)
    cbar1 = colorbar(cax=cax1, orientation='horizontal', label=lbl_vd.displayname+' [%s]'%(lbl))
    #cbar1.ax.set_xlabel('oto')
    #colorbar(label=lbl)
    #imshow(map_mean, origin='lower')
    #title(lbl_vd.displayname)
    #suptitle(lbl_vd.displayname)

    ax_clim = getVarAxLimit(varname, axes_limit)
    if ax_clim:
        clim(*ax_clim)


def basemap_df(idf, varname, lonname='lon', latname='lat', method='mean', regrid_res=0.5,\
                clearfig=True, vd=None, axes_limit=None, fig=None,extend=None,tit=None,center_zero=False,\
                vmin=None,vmax=None,ax_clim=True, labelColorBar=None, particularCmap=False,cmap_name=None,bbox=None,reso_proj='c',returnmatrix=False):
    """
    it seems that currently particularCmap means 'jet' cmap
    :args:
        bbox tuple : latmin,latmax,lonmin,lonmax
    """
    from scipy import stats
    if clearfig: clf()

    #print center_zero

    if fig is None:
        fig = figure(figsize=(10,6))

    if title is not None:
        title(tit)


    #print vmin,vmax

    if center_zero==True:
        if vmin is None:
            vmax = np.max(np.abs(idf[varname]))
            vmin = -vmax

    ax = fig.gca()

    regrid_res_lon = regrid_res
    #regrid_res_lat = regrid_res*2.
    regrid_res_lat = regrid_res
    if bbox is None:
        lons = np.arange(-180,180+regrid_res_lon,regrid_res_lon)
        lats = np.arange(-90,90+regrid_res_lat,regrid_res_lat)
    else:
        lons = np.arange(bbox[2],bbox[3]+regrid_res_lon,regrid_res_lon)
        lats = np.arange(bbox[0],bbox[1]+regrid_res_lat,regrid_res_lat)
#         print('lon',lons)
    if method=='max':#patch because 'max' keyword deseapeared in version 0.18.1 of scipy (agrouaze August 2019)
        method = np.nanmax
    statistic, xedges, yedges, binnumber = stats.binned_statistic_2d(idf[lonname], idf[latname],
                    values=idf[varname], statistic=method,  bins=[lons, lats])

    if method=='count':
        map_mean = np.ma.array(statistic, mask=(statistic==0)).T
    else:
        map_mean = np.ma.array(statistic, mask=np.isnan(statistic)).T

    if vmin is None:
        vmin=np.min(map_mean)
    if vmax is None:
        vmax=np.max(map_mean)

    #if method=='count':
    #    print vmin, vmax

    from mpl_toolkits.basemap import Basemap
    if bbox is None:
        m = Basemap(resolution=reso_proj,projection='cyl',lat_0=60.)
        parallels = np.arange(-90, 90, 30)
        meridians = np.arange(-180., 180., 45)
    else:
        mS = bbox[0]
        mN = bbox[1]
        mE = bbox[3]
        mW = bbox[2]
        
#         mW=-180
#         mE=180
#         mN=85
#         mS=-85
        parallels = np.arange(-90, 90, 20)
        meridians = np.arange(-180., 180., 20)
#         m = Basemap(projection='merc',llcrnrlat=mS,urcrnrlat=mN,llcrnrlon=mW, urcrnrlon=mE,resolution='i')
        m = Basemap(resolution=reso_proj,projection='cyl',lat_0=60.,llcrnrlat=mS,urcrnrlat=mN,\
            llcrnrlon=mW,urcrnrlon=mE)
    #m.imshow(map_mean, origin='lower')
    if particularCmap == True:
        if cmap_name is not None:
            cm = plt.get_cmap(cmap_name)
        else:
            cm = plt.cm.jet
        if center_zero==True:
            #print varname
            #print vmin, vmax
            m.pcolormesh(xedges, yedges, map_mean,vmin=vmin,vmax=vmax,cmap=cm)
        else:
            m.pcolormesh(xedges, yedges, map_mean,vmin=vmin,vmax=vmax,cmap=cm)
    else:
        if center_zero==True:
                #print varname
                #print vmin, vmax
            m.pcolormesh(xedges, yedges, map_mean,vmin=vmin,vmax=vmax)
        else:
#             print(map_mean.mean())
#             print(xedges)
#             print(yedges)
            if bbox is None:
                m.pcolormesh(xedges, yedges, map_mean,vmin=vmin,vmax=vmax)
            else:
                xxe,yye = m(xedges,yedges)
                m.pcolormesh(xxe, yye, map_mean,vmin=vmin,vmax=vmax)
#             

    #xx, yy = np.meshgrid(lats, lons)
    #yy, xx = m(yy, xx)
    #m.scatter(xx, yy, 1, c=map_mean, lw=0)
    m.drawcoastlines(linewidth=0.2)
    #m.fillcontinents(color='coral', lake_color='aqua', alpha=alpha)
    m.fillcontinents(color='0.1', alpha=0.2)
    if particularCmap:
        m.drawmapboundary(fill_color='green')
    
    
    m.drawparallels(parallels,labels=[1,0,0,0],dashes = [1,4])
    m.drawmeridians(meridians,labels=[0,0,0,1],dashes = [1,4])

    lbl = ''
    lbl_vd = getVarDisplay(varname,vd)
    if lbl_vd.unit:
        lbl = lbl_vd.unit

    divider = make_axes_locatable(ax)
    cax1 = divider.append_axes("bottom", size="5%", pad=.5)

    if labelColorBar is None:
        if method=='count':
            label = 'count'
        else:
            label = lbl_vd.displayname+' [%s]'%(lbl)
    else:
        label = labelColorBar
    if extend is not None:
        cbar1 = colorbar(cax=cax1, orientation='horizontal', label=label, extend=extend)
    else:
        cbar1 = colorbar(cax=cax1, orientation='horizontal', label=label)
    #cbar1.ax.set_xlabel('oto')
    #colorbar(label=lbl)
    #imshow(map_mean, origin='lower')
    #title(lbl_vd.displayname)
    #suptitle(lbl_vd.displayname)


    if method is not 'count':
        if ax_clim==True:
            ax_clim = getVarAxLimit(varname, axes_limit)
            if ax_clim:
                clim(*ax_clim)
    if returnmatrix:
        return fig,map_mean,xedges,yedges
    else:
        return fig

def mean_angle(deg, weight=1):
    from cmath import rect, phase
    toto = [rect(1, np.deg2rad(deg_i)) for deg_i in deg] 
    intermed = np.sum(weight * toto) / len(deg)
    return np.rad2deg(phase(intermed))

def is_invertible(a):
    return a.shape[0] == a.shape[1] and np.linalg.matrix_rank(a) == a.shape[0]

def plot_scat_dens(_x,_y, xlabel=None, ylabel=None, title=None, outname=None, xlimits=None, ylimits=None,
                    nb_point=20., mean_angle_bool=False, smooth_factor=0., filt=None, ortho=False,
                     zero_line=False, print_stat=False, unit=None, dif=False, linear_fit=False,
                     xbin=1,cmap=None,angle_values=False,gaussian_fit=False,ax=None,offx=1.2,
                     fontsize=15,cb_label='point density estimate using Gaussian kernels.',legend_disp=True,
                   return_stat=False,mean_curve=True,logdensity=False,add_points=False):
    """
    code from Romain husson to plot smart Q/Q plots with stats and so on.
    :Args:
        xlimits (tuple/list):
        ylimits (tuple/list):
        outname (str): useless
        nb_point (int): resolution of the colorbar of the fit per bin
        filt (boolean array): to filter the x and y vector
        print_stat (bool):
        xbin (int): step of the fit per bin
        angle_values (bool): if the x and y vectors are angles between 0 and 360 stats are computed differently
        offx (float): x starting percentage of figure for annotated stats + legend
    :Returns:
        fig (plt obj): figure
    """
    vec_x = None
    mean_y = None
    if filt != None:
        x = _x[filt]
        y = _y[filt]
    else:
        x = _x.copy()
        y = _y.copy()          
    
    x_init = x.copy()
    y_init = y.copy()

    if x.shape[0] > 15000:
        xratio = int(x.shape[0]/15000)
        x_init = x.copy()
        y_init = y.copy()          
        x = x[::xratio]
        y = y[::xratio]
    
    ## wave_pal = '/home/rhusson/devels/coloc_sar_ww3/wave_spec.pal'
    ## cmap = getColorMap( rgbFile = wave_pal )
    if ax is None:
        fig = plt.figure(figsize=(10,8))
    else:
        
        plt.sca(ax)
        fig = plt.gcf()
    plt.grid(True)
    if add_points:
        plt.plot(x_init,y_init,'k.',ms=1,alpha=0.3)
    xy = np.vstack([x,y])
#     isinvertible = is_invertible(xy)
    try:

        z = gaussian_kde(xy)(xy)
        if logdensity:
            z = np.log10(z)
    except:
        logging.error('%s',traceback.format_exc())
        logging.info('plot_scat_dens() | singular matrix gaussian_kde')
        z = np.zeros(x.shape)
    idx = z.argsort()
    x, y, z = x[idx], y[idx], z[idx]
    if xlabel != None:
        plt.xlabel(xlabel, fontsize=fontsize)
    if ylabel != None:
        plt.ylabel(ylabel, fontsize=fontsize)
    if title != None:
        plt.title(title,fontsize=fontsize)
    ## if ortho & (xlimits is None & ylimits is None):
    ##      xlimits = [np.min(xy), np.max(xy)]
    ##      ylimits = xlimits
    if xlimits is None:
        xlimits = [np.min(x), np.max(x)]
    if ylimits is None:
        ylimits = [np.min(y), np.max(y)]
    plt.xlim(xlimits)
    plt.ylim(ylimits)
    plt.scatter(x, y, c=z, s=20, edgecolor='k', cmap=cmap,lw=0)
    cb = plt.colorbar(label=cb_label)#title='Point Density')
#     cb.fontsize(6)
    font = matplotlib.font_manager.FontProperties(family='times new roman', style='italic', size=12)
    
    text = cb.ax.yaxis.label
    text.set_font_properties(font)
    cb.ax.tick_params(labelsize=fontsize)
    cb.outline.set_visible(False)
    cb.set_ticks([])
    
    step_hist = (max(x)-min(x))/(nb_point+1)
    logging.debug('plot_scat_dens | step_hist %s',step_hist)
    if step_hist>0 and mean_curve is True: #~security added by agrouaze sept 2019 in case X contains only zeros values
        vec_x = np.arange(min(x) + step_hist/2, max(x) - step_hist/2, step_hist)
        mean_y = np.zeros_like(vec_x)
        median_y = np.zeros_like(vec_x)
        for _i, x_ii in enumerate(vec_x):
            ind = np.logical_and(x < (x_ii+step_hist/2 + smooth_factor * step_hist), x > (x_ii-step_hist/2 - smooth_factor * step_hist))
            if len(ind) == 0:
                mean_y[_i] = False
                median_y[_i] = False
                continue
            if mean_angle_bool:
                mean_y[_i] = mean_angle(y[ind])
                median_y[_i] = mean_angle(y[ind]) #to find equivalent but with median for angles?
            else:
                median_y[_i] = np.median(y[ind])
                mean_y[_i] = np.mean(y[ind])
        plt.plot(vec_x, mean_y, 'black',label='mean Y values per %1.1f bin'%(step_hist))
        plt.plot(vec_x, median_y,'.--',label='median Y values per %1.1f bin'%(step_hist))
    if ortho:
        plt.plot(xlimits, ylimits, 'magenta', linestyle='dashed',label='orthogonal line')
    if zero_line:
        plt.plot(xlimits, [0, 0], 'black', linestyle='dashed',label='zero line')
              
    if linear_fit:
        m_lin,b_lin = polyfit(x_init, y_init, 1) 
        xlimits = np.array(xlimits)
        plt.plot(xlimits, m_lin*xlimits+b_lin, 'black', linestyle='dashdot',label='linear fit %1.2fx+%1.2f'%(m_lin,b_lin))
    if gaussian_fit:
        sig = 100

        ind_sort_x = np.argsort(x)
        sorted_y = y[ind_sort_x]
        sorted_x = x[ind_sort_x]
        filtered = gaussian_filter1d(sorted_y,sigma=sig)
        
        plt.plot(sorted_x,filtered,'r-',lw=2,label='gaussian filter sigma=%s'%(sig),alpha=0.7)

    #         offx = 0.03
    if print_stat:
        if dif:
            bias = np.mean(y_init)
            rmse = np.sqrt(moment(y_init, moment=2))
        else:
            if angle_values:
                logging.debug('angle values')
                difference = wind_direction_comparison_core(y_init,x_init)
            else:
                difference = y_init-x_init
            bias = np.mean(difference)
            rmse = np.sqrt(moment(difference, moment=2))
            cor, pval = pearsonr(x_init, y_init)
            SI = rmse/np.mean(x_init)*100 #coefficient of variation https://stats.stackexchange.com/questions/26863/what-is-the-rmse-normalized-by-the-mean-observed-value-called
            
        nb = x_init.size
        step = 0.045
        offy = 0.05
        fontsize=fontsize
        bg_c = 'white'
        effect = [path_effects.withSimplePatchShadow()]
        effect = []
        text_color='k'
        plt.text(offx, 1-offy, "Bias: %.3f"%(bias) + unit,{'color': text_color}, transform=plt.gca().transAxes,path_effects=effect,fontsize=fontsize,backgroundcolor=bg_c)
        plt.text(offx, 1-offy-step*1, "RMSE: %.3f"%(rmse) + unit,{'color': text_color}, transform=plt.gca().transAxes,path_effects=effect,fontsize=fontsize,backgroundcolor=bg_c)
        plt.text(offx, 1-offy-step*2, "N: %i"%(nb),{'color': text_color}, transform=plt.gca().transAxes,path_effects=effect,fontsize=fontsize,backgroundcolor=bg_c)
        if linear_fit & (dif is True):
            plt.text(offx, 1-offy-step*3, "Lin.reg: y=%.4fx+%.2f"%(m_lin,b_lin),{'color': text_color}, transform=plt.gca().transAxes,path_effects=effect,fontsize=fontsize,backgroundcolor=bg_c)
        if linear_fit & (dif is False):
            plt.text(offx, 1-offy-step*5, "Lin.reg: y=%.4fx+%.2f"%(m_lin,b_lin),{'color': text_color}, transform=plt.gca().transAxes,path_effects=effect,fontsize=fontsize,backgroundcolor=bg_c)
        if dif is False:
            plt.text(offx, 1-offy-step*3, "SI: %.2f"%(SI) + '%',{'color': text_color}, transform=plt.gca().transAxes,path_effects=effect,fontsize=fontsize,backgroundcolor=bg_c)
            plt.text(offx, 1-offy-step*4, "R: %.2f"%(cor),{'color': text_color}, transform=plt.gca().transAxes,path_effects=effect,fontsize=fontsize,backgroundcolor=bg_c)
        # Create a Rectangle patch
        #rectleft = xlimits[0]
        #rectright = ylimits[0]
        #rect_width = (xlimits[1]-xlimits[0])*0.3
        #rect_height = (ylimits[1]-ylimits[0])*0.3
        #rect = patches.Rectangle((rectleft,rectright),rect_width,rect_height,linewidth=1,edgecolor='r',facecolor='white',alpha=0.5)

        # Add the patch to the Axes
        #ax = plt.gca()
        #ax.add_patch(rect)
    if legend_disp:
        plt.legend(bbox_to_anchor=(offx+0.3, 0.3),fontsize=8)
    del offx
    if return_stat:
        return fig,bias,rmse,SI,nb,cor,vec_x,mean_y
    else:
        return fig
